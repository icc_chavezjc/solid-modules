package org.nextiva.modular.core.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
class IndexController {

    @GetMapping
    String sayHello(@RequestParam String name) {
        log.info("Message for {}", name);
        return String.format("Hello %s!", name);
    }
}
