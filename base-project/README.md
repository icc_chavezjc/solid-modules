# Add Support for Boot Loader
## Checklist

 1. In the parent POM add
    1. Add the follow `properties`
        ```
        <spring-boot-loader.version>2.4.3</spring-boot-loader.version>
        <manifest.file>src/main/resources/MANIFEST.MF</manifest.file>
        ```
       With the details of the configuration for dependencies and plugins.
    1. In the `dependency` section
        ```
            <dependency>
              <groupId>org.springframework.boot</groupId>
              <artifactId>spring-boot-loader</artifactId>
              <version>${spring-boot-loader.version}</version>
            </dependency>
        ```
       With the Spring Boot Loader dependency.
    1. Add the `build` section
        ```
        <build>
                <plugins>
                    <plugin>
                        <groupId>org.springframework.boot</groupId>
                        <artifactId>spring-boot-maven-plugin</artifactId>
                        <configuration>
                            <executable>true</executable>
                            <mainClass>org.springframework.boot.loader.PropertiesLauncher</mainClass>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-jar-plugin</artifactId>
                        <version>3.1.0</version>
                        <executions>
                            <execution>
                                <id>default-jar</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                                <configuration>
                                    <archive>
                                        <manifestFile>${manifest.file}</manifestFile>
                                    </archive>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        ```
       Prepares the JAR with all the dependencies required.
   1. Create the file `src/main/resources/MANIFEST.MF`, with the follow contents:
        ```
        Main-Class: org.springframework.boot.loader.PropertiesLauncher
        ```
      This will indicate the Boot Loader to use, in our case the `PropertiesLauncher`, which allows to configure the launch details with properties files, once everything has been configured, the "application" Main class used.
   1. Create the file `loader.properties` (under root), with the follow contents:
        ```
        loader.main=org.nextiva.modular.core.Applications
        ```
      This will indicate where is the Application main class.