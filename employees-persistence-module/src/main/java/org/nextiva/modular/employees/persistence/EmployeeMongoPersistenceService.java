package org.nextiva.modular.employees.persistence;

import org.nextiva.modular.employees.EmployeeDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class EmployeeMongoPersistenceService implements EmployeePersistenceService {

    private final EmployeeRepository employeeRepository;

    EmployeeMongoPersistenceService(EmployeeRepository employeeRepository) {
        log.info("Loading {}", this.getClass().getCanonicalName());
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeDto save(EmployeeDto employee) {
        log.info("Saving employee in mongo...");
        Employee saved = employeeRepository.save(map(employee));
        return map(saved);
    }

    @Override
    public EmployeeDto find(String id) {
        log.info("Searching employee in mongo...");
        return employeeRepository.findById(id)
                .map(this::map)
                .orElse(null);
    }

    @Override
    public boolean remove(String id) {
        if (!employeeRepository.existsById(id)) {
            return false;
        }

        employeeRepository.deleteById(id);
        return true;
    }

    private Employee map(EmployeeDto employee) {
        Employee model = new Employee();
        model.setId(employee.getId());
        model.setFirstName(employee.getFirstName());
        model.setLastName(employee.getLastName());
        model.setHourRate(employee.getHourRate());
        model.setHireDate(employee.getHireDate());
        return model;
    }

    private EmployeeDto map(Employee employee) {
        EmployeeDto dto = new EmployeeDto();
        dto.setId(employee.getId());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setHourRate(employee.getHourRate());
        dto.setHireDate(employee.getHireDate());
        return dto;
    }
}
