package org.nextiva.modular.employees.persistence;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
class Employee {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate hireDate;
    private BigDecimal hourRate;
}
