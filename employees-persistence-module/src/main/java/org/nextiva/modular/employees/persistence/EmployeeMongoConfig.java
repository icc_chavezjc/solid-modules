package org.nextiva.modular.employees.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Configuration
@EnableMongoRepositories
class EmployeeMongoConfig {

    private final EmployeeRepository employeeRepository;

    @Bean
    @Primary
    EmployeePersistenceService mongoEmployeePersistenceService() {
        log.info("Mongo Employee Persistence Service");
        return new EmployeeMongoPersistenceService(employeeRepository);
    }
}
