package org.nextiva.modular.employees.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
interface EmployeeRepository extends MongoRepository<Employee, String> {
}
