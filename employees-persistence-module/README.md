# Mongo Employee Persistence Module


1. Add an "extension" module that store in MongoDB the Employees instead of memory.
    - `Employee` Model, with the configurations for the Document.
    - `EmployeeRepository` a Mongo Repository interface to provide the basic functionality for Mongo.
    -  `EmployeeMongoPersistenceService` providing the bridge between the Controller and the Repository.
    - `EmployeeMongoConfig` providing the configuration for the Mongo (`@EnableMongoRepositories`) and the `@Primary` `EmployeePersistenceService` bean (to be loaded as first option, instead of the "default")
    
 # Pre-requisites
 - A MongoDB instance (installed, configured and running)
    - [Install MongoDB in Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
    - [Install MongoDB in Linux](https://docs.mongodb.com/manual/administration/install-on-linux/)
    - [Install MongoDB in MacOS](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/)
    - [Using MongoDB from a Docker Container](https://www.thepolyglotdeveloper.com/2019/01/getting-started-mongodb-docker-container-deployment/)
  - A MongoDB with the follow configuration:
    - database: sample
    - host: localhost
    - port: 27017
  - MongoDB running in the default port ()

## Configuration
   1. The POM is a little different, since we need a JAR with the dependencies for Spring Data - Mongo (which are not in the base project)
         ```
                        <plugin>
                            <artifactId>maven-assembly-plugin</artifactId>
                            <version>3.3.0</version>
                            <executions>
                                <execution>
                                    <phase>install</phase>
                                    <goals>
                                        <goal>single</goal>
                                    </goals>
                                </execution>
                            </executions>
                            <configuration>
                                <outputDirectory>../base-project/target/libs</outputDirectory>
                                <appendAssemblyId>false</appendAssemblyId>
                                <descriptorRefs>
                                    <descriptorRef>jar-with-dependencies</descriptorRef>
                                </descriptorRefs>
                            </configuration>
                        </plugin>
         ```
        This section will create a JAR with all the dependencies and copying it to the `libs` dir, during the `install` phase.
   1. An `application.yml` with the MongoDB config data
   
## EmployeePersistenceService Configuration
   1. Provide a Config class to set up Spring Data Mongo, with:
        - `@Configuration`
        - `@EnableMongoRepositories`
        - A method with: `@Bean` and `@Primary`
    
   Since the `employees-module` provides a  `EmployeePersistenceService`, we need to indicate to Spring to use the new implementation instead of the `default` one. This is done by adding.
                

