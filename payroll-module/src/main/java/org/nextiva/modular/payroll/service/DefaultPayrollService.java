package org.nextiva.modular.payroll.service;

import java.math.BigDecimal;

import org.nextiva.modular.employees.EmployeeDto;
import org.nextiva.modular.employees.persistence.EmployeePersistenceService;
import org.nextiva.modular.payroll.web.PayrollRequestDto;
import org.nextiva.modular.payroll.web.PayrollResponseDto;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;

import static java.util.Objects.isNull;

@Slf4j
@Service
@ConditionalOnMissingBean(type = {"PayrollService"})
class DefaultPayrollService implements PayrollService {

    private final EmployeePersistenceService employeePersistenceService;

    DefaultPayrollService(
            EmployeePersistenceService employeePersistenceService) {
        log.info("Default Payroll Service loaded: {}", this.getClass().getCanonicalName());
        this.employeePersistenceService = employeePersistenceService;
    }

    @Override
    public PayrollResponseDto calculate(PayrollRequestDto payrollRequest) {
        log.info("Calculate Payroll for [employeeID={}]", payrollRequest.getEmployeeId());
        PayrollResponseDto payrollResponse = createPayrollResponse(payrollRequest);
        calculateSalary(payrollResponse);
        return payrollResponse;
    }

    private PayrollResponseDto createPayrollResponse(PayrollRequestDto payrollRequest) {
        PayrollResponseDto payrollResponse = new PayrollResponseDto();

        payrollResponse.setEmployeeId(payrollRequest.getEmployeeId());
        payrollResponse.setHoursWorkedMonthly(payrollRequest.getHoursWorkedMonthly());
        payrollResponse.setPeriod(payrollRequest.getPeriod());
        payrollResponse.setTaxRate(payrollRequest.getTaxRate());

        return payrollResponse;
    }

    private void calculateSalary(PayrollResponseDto payrollResponse) {
        EmployeeDto employee = employeePersistenceService.find(payrollResponse.getEmployeeId());

        if (isNull(employee)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee not found");
        }

        BigDecimal hoursWorked = BigDecimal.valueOf(payrollResponse.getHoursWorkedMonthly());

        BigDecimal salary = employee.getHourRate().multiply(hoursWorked);
        BigDecimal taxes = payrollResponse.getTaxRate().multiply(salary);

        payrollResponse.setMonthlySalary(salary);
        payrollResponse.setTaxes(taxes);
        payrollResponse.setSalaryAfterTaxes(salary.subtract(taxes));
    }
}
