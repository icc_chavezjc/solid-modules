package org.nextiva.modular.payroll.web;

import java.math.BigDecimal;
import java.time.YearMonth;

import lombok.Data;

@Data
public class PayrollResponseDto {
    private String employeeId;
    private Double hoursWorkedMonthly;
    private YearMonth period;
    private BigDecimal taxRate;
    private BigDecimal monthlySalary;
    private BigDecimal taxes;
    private BigDecimal salaryAfterTaxes;
}
