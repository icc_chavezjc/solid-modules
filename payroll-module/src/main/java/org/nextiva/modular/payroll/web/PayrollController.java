package org.nextiva.modular.payroll.web;

import org.nextiva.modular.payroll.service.PayrollService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/payroll")
class PayrollController {

    private final PayrollService payrollService;

    @PostMapping
    PayrollResponseDto calculatePayroll(@RequestBody PayrollRequestDto payrollRequest) {
        return payrollService.calculate(payrollRequest);
    }
}
