package org.nextiva.modular.payroll.service;

import org.nextiva.modular.payroll.web.PayrollRequestDto;
import org.nextiva.modular.payroll.web.PayrollResponseDto;

public interface PayrollService {
    PayrollResponseDto calculate(PayrollRequestDto payrollRequest);
}
