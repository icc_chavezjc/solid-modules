# Employee Services
   The module provides the employee functionality to:
   - Create / Update
   - Search by ID
   - Remove by ID

   ## Notes about the Persistence Service
   - An interface for the persistence layer is present, with a default implementation for testing purposes.
   - The default Persistence Service uses Map to store the employees, so there is only InMemory functionality.
   - This default service will be used only if there is not found any other implementation for the `EmployeePersistenceService`. This is configured thru: `@ConditionalOnMissingBean(type = {"EmployeePersistenceService"})` in: `org.nextiva.modular.employees.persistence.EmployeeDefaultPersistenceService`    
   
   ## NOTES about the BUILD 
   1. The JAR build for this module does not include any dependency, those must be provided by the application loading this module.
   1. The JAR created will be directly saved in the `../base-project/target/libs` directory to avoid doing the process manually.