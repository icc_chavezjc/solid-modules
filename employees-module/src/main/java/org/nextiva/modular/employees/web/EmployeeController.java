package org.nextiva.modular.employees.web;

import org.nextiva.modular.employees.EmployeeDto;
import org.nextiva.modular.employees.persistence.EmployeePersistenceService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.extern.slf4j.Slf4j;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Slf4j
@RestController
@RequestMapping(value = "/employees")
class EmployeeController {

    private final EmployeePersistenceService employeePersistenceService;

    EmployeeController(EmployeePersistenceService employeePersistenceService) {
        log.info("Loading {}", this.getClass().getCanonicalName());
        this.employeePersistenceService = employeePersistenceService;
    }

    @PostMapping
    EmployeeDto createEmployee(@RequestBody EmployeeDto employee) {
        return employeePersistenceService.save(employee);
    }

    @GetMapping("/{employee-id}")
    EmployeeDto findEmployee(@PathVariable(value = "employee-id") String employeeId) {
        EmployeeDto employeeDto = employeePersistenceService.find(employeeId);
        if (isNull(employeeDto)) {
            throw new ResponseStatusException(NOT_FOUND, "Employee not found");
        }

        return employeeDto;
    }

    @ResponseStatus(ACCEPTED)
    @DeleteMapping("/{employee-id}")
    void deleteEmployee(@PathVariable(value = "employee-id") String employeeId) {
        if (!employeePersistenceService.remove(employeeId)) {
            throw new ResponseStatusException(NOT_FOUND, "Employee not found");
        }
    }
}
