package org.nextiva.modular.employees.persistence;

import org.nextiva.modular.employees.EmployeeDto;

public interface EmployeePersistenceService {
    EmployeeDto save(EmployeeDto employee);
    EmployeeDto find(String id);
    boolean remove(String id);
}
