package org.nextiva.modular.employees.persistence;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.nextiva.modular.employees.EmployeeDto;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * This is the Default Persistence Service for Employees, implementing InMemory (Map based).
 * The @ConditionalOnMissingBean will ensure this Service is not used, if other implementation of
 * EmployeePersistenceService is found.
 */
@Slf4j
@Service
@ConditionalOnMissingBean(type = {"EmployeePersistenceService"})
class EmployeeDefaultPersistenceService implements EmployeePersistenceService {

    private final Map<String, EmployeeDto> employees = new HashMap<>();

    public EmployeeDefaultPersistenceService() {
        log.info("Loading {}", this.getClass().getCanonicalName());
    }

    @Override
    public EmployeeDto save(EmployeeDto employee) {
        log.info("Saving in memory");
        employee.setId(UUID.randomUUID().toString());
        employees.put(employee.getId(), employee);
        return employee;
    }

    @Override
    public EmployeeDto find(String id) {
        log.info("Searching in memory");
        if (!employees.containsKey(id)) {
            return null;
        }

        return employees.get(id);
    }

    @Override
    public boolean remove(String id) {
        log.info("Removing from memory");
        if (employees.containsKey(id)) {
            employees.remove(id);
            return true;
        }

        return false;
    }
}
