package org.nextiva.modular.employees;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

@Data
public class EmployeeDto {
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate hireDate;
    private BigDecimal hourRate;
}
