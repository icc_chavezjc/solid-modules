# Build & Run the Project
   To add the capabilities to run with load at boot time support
   1. Build 
        ```
        mvn clean install
        ```
   1. Execute
        ```
        java \ 
            -Dloader.debug=true \
            -Dloader.path=file:./target/libs/ \
            -jar base-project/target/base-project-0.1.0-SNAPSHOT.jar
       ```
   Where:
   - `-Dloader.debug=true`
    Display the details of the execution, very useful for debugging.
   - 

# Execute

`curl --location --request GET 'http://localhost:8080?name=John'`
